package org.apereo.cas.customlogout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apereo.cas.authentication.AuthenticationServiceSelectionPlan;
import org.apereo.cas.authentication.principal.WebApplicationService;
import org.apereo.cas.logout.DefaultLogoutRequest;
import org.apereo.cas.logout.DefaultSingleLogoutServiceMessageHandler;
import org.apereo.cas.logout.LogoutHttpMessage;
import org.apereo.cas.logout.LogoutMessageCreator;
import org.apereo.cas.logout.LogoutRequest;
import org.apereo.cas.logout.LogoutRequestStatus;
import org.apereo.cas.logout.SingleLogoutServiceLogoutUrlBuilder;
import org.apereo.cas.logout.SingleLogoutServiceMessageHandler;
import org.apereo.cas.services.LogoutType;
import org.apereo.cas.services.RegisteredService;
import org.apereo.cas.services.ServicesManager;
import org.apereo.cas.util.http.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomSingleLogoutServiceMessageHandler implements SingleLogoutServiceMessageHandler{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSingleLogoutServiceMessageHandler.class);

    private final ServicesManager servicesManager;
    private final HttpClient httpClient;
    private boolean asynchronous = true;
    private final LogoutMessageCreator logoutMessageBuilder;
    private final SingleLogoutServiceLogoutUrlBuilder singleLogoutServiceLogoutUrlBuilder;
    private final AuthenticationServiceSelectionPlan authenticationRequestServiceSelectionStrategies;

    /**
     * Instantiates a new Single logout service message handler.
     *
     * @param httpClient                                      to send the requests
     * @param logoutMessageCreator                            creates the message
     * @param servicesManager                                 finds services to logout from
     * @param singleLogoutServiceLogoutUrlBuilder             creates the URL
     * @param asyncCallbacks                                  if messages are sent in an asynchronous fashion.
     * @param authenticationRequestServiceSelectionStrategies the authentication request service selection strategies
     */
    public CustomSingleLogoutServiceMessageHandler(final HttpClient httpClient, final LogoutMessageCreator logoutMessageCreator,
                                                    final ServicesManager servicesManager,
                                                    final SingleLogoutServiceLogoutUrlBuilder singleLogoutServiceLogoutUrlBuilder,
                                                    final boolean asyncCallbacks,
                                                    final AuthenticationServiceSelectionPlan authenticationRequestServiceSelectionStrategies) {
        this.httpClient = httpClient;
        this.logoutMessageBuilder = logoutMessageCreator;
        this.servicesManager = servicesManager;
        this.singleLogoutServiceLogoutUrlBuilder = singleLogoutServiceLogoutUrlBuilder;
        this.asynchronous = asyncCallbacks;
        this.authenticationRequestServiceSelectionStrategies = authenticationRequestServiceSelectionStrategies;
    }

    /**
     * Handle logout for slo service.
     *
     * @param singleLogoutService the service
     * @param ticketId            the ticket id
     * @return the logout request
     */
    @Override
    public LogoutRequest handle(final WebApplicationService singleLogoutService, final String ticketId) {
        if (singleLogoutService.isLoggedOutAlready()) {
            LOGGER.debug("Service [{}] is already logged out.", singleLogoutService);
            return null;
        }

        final WebApplicationService selectedService = WebApplicationService.class.cast(
                this.authenticationRequestServiceSelectionStrategies.resolveService(singleLogoutService));

        LOGGER.debug("Processing logout request for service [{}]...", selectedService);
        final RegisteredService registeredService = this.servicesManager.findServiceBy(selectedService);

        if (!serviceSupportsSingleLogout(registeredService)) {
            LOGGER.debug("Service [{}] does not support single logout.", selectedService);
            return null;
        }
        LOGGER.debug("Service [{}] supports single logout and is found in the registry as [{}]. Proceeding...", selectedService, registeredService);
        
//        URL logoutUrl = null;
//		try {
//			logoutUrl = new URL("https://app2/logout");
//		} catch (MalformedURLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
        
        //LOGGER.debug("Prepared logout url [{}] for service [{}]", logoutUrl, selectedService);
//        if (logoutUrl == null) {
//            LOGGER.debug("Service [{}] does not support logout operations given no logout url could be determined.", selectedService);
//            return null;
//        }

        LOGGER.debug("Creating logout request for [{}] and ticket id [{}]", selectedService, ticketId);
        final DefaultLogoutRequest logoutRequest = new DefaultLogoutRequest(ticketId, selectedService, registeredService.getLogoutUrl());
        LOGGER.debug("Logout request [{}] created for [{}] and ticket id [{}]", logoutRequest, selectedService, ticketId);

       // final LogoutType type = registeredService.getLogoutType() == null ? LogoutType.BACK_CHANNEL : registeredService.getLogoutType();
       // LOGGER.debug("Logout type registered for [{}] is [{}]", selectedService, type);
        try {
			if (performAjaxCall(selectedService,logoutRequest)) {
			    logoutRequest.setStatus(LogoutRequestStatus.SUCCESS);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//        switch (type) {
//            case BACK_CHANNEL:
//                if (performBackChannelLogout(logoutRequest)) {
//                    logoutRequest.setStatus(LogoutRequestStatus.SUCCESS);
//                } else {
//                    logoutRequest.setStatus(LogoutRequestStatus.FAILURE);
//                    LOGGER.warn("Logout message is not sent to [{}]; Continuing processing...", singleLogoutService.getId());
//                }
//                break;
//            default:
//                LOGGER.debug("Logout operation is not yet attempted for [{}] given logout type is set to [{}]", selectedService, type);
//                logoutRequest.setStatus(LogoutRequestStatus.NOT_ATTEMPTED);
//                break;
//        }
        return logoutRequest;
        //LR-1-YHTJ0QiruL6j3vKlckZPGo1pok7ruMFOU9z
        //ST-1-whpVqRo7F1BfW7Edp9lH-DESKTOP-T4BVCMB
        //ST-2-fVtB2PzTsvJv26pmiEKr-DESKTOP-T4BVCMB
        //ST-2-fVtB2PzTsvJv26pmiEKr-DESKTOP-T4BVCMB

    }
    
//    public boolean performAjaxCall(WebApplicationService selectedService, final LogoutRequest request) throws ClientProtocolException, IOException{
//    	String[] split = selectedService.getOriginalUrl().split("/");
//    	String logoutUrl = split[0] + "//" + split[2] + "/signout";
//    	//URL logoutUrl = request.getLogoutUrl();    	
//
//    	org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
//    	
//        HttpPost httpPost = new HttpPost(logoutUrl);
//     
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("SessionIndex", request.getTicketId()));
//        httpPost.setEntity(new UrlEncodedFormEntity(params));  	
//
//    	HttpResponse response = client.execute(httpPost);
//
//    	System.out.println("Response Code : "
//    	                + response.getStatusLine().getStatusCode());
//
//    	BufferedReader rd = new BufferedReader(
//    		new InputStreamReader(response.getEntity().getContent()));
//
//    	StringBuffer result = new StringBuffer();
//    	String line = "";
//    	while ((line = rd.readLine()) != null) {
//    		result.append(line);
//    	}
//		return asynchronous;   	
//    
//    }
    
    
  public boolean performAjaxCall(WebApplicationService selectedService, final LogoutRequest request) throws ClientProtocolException, IOException{
  	String[] split = selectedService.getOriginalUrl().split("/");
  	String logoutUrl = split[0] + "//" + split[2] + "/signout?SessionIndex=" + request.getTicketId();
  	//String logoutUrl = "https://localhost:8445" + "/signout?SessionIndex=" + request.getTicketId();
  	 

    org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
    HttpGet httpGet = new HttpGet(logoutUrl);

    // add request header
    HttpResponse response = client.execute(httpGet);

    System.out.println("Response Code : "
                    + response.getStatusLine().getStatusCode());

    BufferedReader rd = new BufferedReader(
    	new InputStreamReader(response.getEntity().getContent()));

    StringBuffer result = new StringBuffer();
    String line = "";
    while ((line = rd.readLine()) != null) {
    	result.append(line);
    }
	return asynchronous;
  	
  	
//  	SSLContext sslContext = null;
//	try {
//		sslContext = new SSLContextBuilder()
//		      .loadTrustMaterial(null, (certificate, authType) -> true).build();
//	} catch (KeyManagementException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	} catch (NoSuchAlgorithmException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	} catch (KeyStoreException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//  	 
//  	    CloseableHttpClient client = HttpClients.custom()
//  	      .setSSLContext(sslContext)
//  	      .setSSLHostnameVerifier(new NoopHostnameVerifier())
//  	      .build();
//  	    HttpGet httpGet = new HttpGet(logoutUrl);
//  	    httpGet.setHeader("Accept", "application/xml");
//  	 
//  	    HttpResponse response = client.execute(httpGet);
//  	  //  assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
//		return asynchronous;
  
  }


    
    
   
    /**
     * Log out of a service through back channel.
     *
     * @param request the logout request.
     * @return if the logout has been performed.
     */
    public boolean performBackChannelLogout(final LogoutRequest request) {
        try {
            LOGGER.debug("Creating back-channel logout request based on [{}]", request);
            final String logoutRequest = this.logoutMessageBuilder.create(request);
            final WebApplicationService logoutService = request.getService();
            logoutService.setLoggedOutAlready(true);

            LOGGER.debug("Preparing logout request for [{}] to [{}]", logoutService.getId(), request.getLogoutUrl());
            final LogoutHttpMessage msg = new LogoutHttpMessage(request.getLogoutUrl(), logoutRequest, this.asynchronous);
            LOGGER.debug("Prepared logout message to send is [{}]. Sending...", msg);
            return this.httpClient.sendMessageToEndPoint(msg);
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * Service supports back channel single logout?
     * Service must be found in the registry. enabled and logout type must not be {@link LogoutType#NONE}.
     *
     * @param registeredService the registered service
     * @return true, if support is available.
     */
    private static boolean serviceSupportsSingleLogout(final RegisteredService registeredService) {
        return registeredService != null
                && registeredService.getAccessStrategy().isServiceAccessAllowed()
                && registeredService.getLogoutType() != LogoutType.NONE;
    }

    public ServicesManager getServicesManager() {
        return this.servicesManager;
    }

}
